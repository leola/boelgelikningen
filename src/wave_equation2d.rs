use core::fmt;
use crate:: matrix::Matrix;

pub struct WaveEquation2D {
    mat: Vec<Matrix>,
    pub size: usize,
    pub time_steps: usize,
    pub h: f32,
    pub k: f32,
}

impl WaveEquation2D {
    pub fn new(size: usize, total_time: usize, k: f32, h: f32) -> WaveEquation2D {
        let empty_matrix: Matrix = Matrix::new_empty(size, size);
        let mut mat = vec![empty_matrix];
        mat.reserve(total_time);
        return WaveEquation2D { 
            mat, 
            size, time_steps: total_time, h, k}
    }
    pub fn initial_values(mut self, f: fn(usize, usize, usize, f32) -> f32) -> WaveEquation2D {
        for i in 1..self.size-1 {
            for j in 1..self.size-1 {
                let val_ij: f32 = f(i, j, self.size, self.h);
                self.mat[0].mat[i][j] = val_ij;
            }
        }
        return self;
    }

    pub fn initial_speed(mut self, g: fn(usize, usize, usize, f32) -> f32) -> WaveEquation2D {
        let mut mat: Matrix = Matrix::new_empty(self.size, self.size);
        for i in 1..self.size-1 {
            for j in 1..self.size-1 {
                let val_ij: f32 = self.mat[0].mat[i][j] 
                                + g(i, j, self.size, self.h);
                mat.mat[i][j] = val_ij;
            }
        }
        self.mat.push(mat);
        return self;
    }

    pub fn solve_wave_equation() {
        use std::f32::consts::E;
        let size = 31;
        let total_time = 1000;
        let k = 0.0001;
        let h = 0.001;
        let solver: WaveEquation2D = WaveEquation2D::new(size, total_time, k, h);
    
        let now = std::time::Instant::now();
        let solver = solver.initial_values(f);   
        println!("{}s:\tSatt inn initialverider", now.elapsed().as_secs());
        let solver = solver.initial_speed(g);
        println!("{}s:\tSatt inn initialfart", now.elapsed().as_secs());
    
        let solver = solver.solve_implicit();
        println!("{}s:\tSatt inn løyst over tid", now.elapsed().as_secs());
        std::fs::write("bolgelikningen.txt", format!("{}", solver)).unwrap();     
    
        fn f(x: usize, y: usize, size: usize, h: f32) -> f32 {
            let middle: f32 = ((size + 1) / 2-1) as f32;
            let x = x as f32 - middle;
            let y = y as f32 - middle;
            let val = (x*x+y*y)/10.0;
            let sin = E.powf(-val)*10.0;
            return sin;
        }
        fn g(x: usize, y: usize, size: usize, h: f32) -> f32 {return 0.0;}
    } 
}

//Solver
impl WaveEquation2D {
    pub fn solve_implicit(mut self) -> WaveEquation2D {
        let now = std::time::Instant::now();
        let solution_matrix: Matrix = self.implicit_soulution_matrix();
        println!("{}s:\tSatt inn laget løysningsmatrise", now.elapsed().as_secs());
        for t in 1..self.time_steps-1 {
            let sum: Matrix = self.mat[t].clone().multiply_scalar(-2.0).add_matrix(self.mat[t-1].clone());
            let sum: Vec<f32> = WaveEquation2D::flatten_matrix(sum);
            let temp: Vec<f32> = solution_matrix.multiply_vector(&sum);
            let temp: Matrix = WaveEquation2D::unflatten_vector(temp, self.size, self.size);
            self.mat.push(temp);
        }
        return self;
    }

    pub fn implicit_soulution_matrix(&self) -> Matrix {
        let mut solution_matrix: Matrix = Matrix::new_empty(self.size*self.size, self.size*self.size);
        let gamma: f32 = self.k*self.k/(self.h*self.h);
        let alpha: f32 = -4.0*gamma - 1.0;
        let beta: f32  = gamma;

        solution_matrix = Self::add_diagonal_element(solution_matrix, alpha, 0);
        solution_matrix = Self::add_diagonal_element(solution_matrix, beta, -1);
        solution_matrix = Self::add_diagonal_element(solution_matrix, beta, 1);
        solution_matrix = Self::add_diagonal_element(solution_matrix, beta, -(self.size as i32));
        solution_matrix = Self::add_diagonal_element(solution_matrix, beta, self.size as i32);
        
        solution_matrix = solution_matrix.inverse_Gauss_Jordan().unwrap();
        //println!("{}", solution_matrix.clone());
        //Syrgje for at randverdiane ikkje blir endra
        for l in 0..self.size {
            solution_matrix.set_row(0.0, l);
            solution_matrix.set_row(0.0,l*self.size)    
        }        
        //println!("\n{}", solution_matrix.clone());

        return solution_matrix;
    }

    fn add_diagonal_element(mut mat: Matrix, value: f32, offset: i32) -> Matrix {
        for i in 0..mat.rows {
            if offset > 0 && i as i32 + offset < mat.rows as i32 {
                mat.mat[i][i + offset as usize] = value;
            } else if offset < 0 && i as i32 + offset >= 0 {
                let offset = (i as i32 + offset) as usize;
                mat.mat[i][offset] = value;
            } else if offset == 0 {
                mat.mat[i][i] = value;
            }
        }
        return mat;
    }
}

//Matrix manipulations
impl WaveEquation2D {
    pub fn flatten_matrix(m: Matrix) -> Vec<f32> {
        let vector_len = m.rows * m.cols;
        let mut flat_matrix: Vec<f32> = Vec::new();
        flat_matrix.reserve(vector_len);
        for i in 0..m.rows {
            let mut row = m.mat[i].clone();
            flat_matrix.append(&mut row);
        }
        return flat_matrix;
    }

    pub fn unflatten_vector(v: Vec<f32>, rows: usize, cols: usize) -> Matrix {
        let mut m: Matrix = Matrix::new_empty(rows, cols);
        for i in 0..rows {
            for j in 0..cols {
                m.mat[i][j] = v[j + cols*i];
            }
        }
        return m;
    }
}


impl fmt::Display for WaveEquation2D {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{x0_points}, {x1_points}, {t_points}, {h}, {k} |",
                 x0_points = self.size, x1_points = self.size, t_points = self.time_steps, h = self.h, k = self.k)?;
        for i in 0..self.time_steps{
            write!(f, "\n{}|", self.mat[i])?;
        }
        Ok(())
    }
}