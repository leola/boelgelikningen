use wave_equation2d::WaveEquation2D;

mod matrix;
mod vector;
mod wave_equation2d;


fn main() {
    WaveEquation2D::solve_wave_equation();
}