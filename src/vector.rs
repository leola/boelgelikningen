use core::fmt;

pub struct Vector {
    pub v: Vec<f32>,
}

impl Vector {
    pub fn new(v: Vec<f32>) -> Vector {
        return Vector{v};
    }

    pub fn sub_vector(&self, rhs: &Self) -> Vector {
        let mut new_vec: Vec<f32> = Vec::new();
        new_vec.reserve(self.v.len());
        for i in 0..self.v.len() {
            new_vec.push(self.v[i] - rhs.v[i]);
        }
        return Vector {v: new_vec};
    }

    pub fn mul_scalar(&self, rhs: f32) -> Vector {
        let mut new_vec: Vec<f32> = Vec::new();
        new_vec.reserve(self.v.len());
        for i in 0..self.v.len() {
            new_vec.push(self.v[i] * rhs);
        }
        return Vector {v: new_vec};
    }

    pub fn dot_product(&self, rhs: Vector) -> f32 {
        print!("{}", self);
        println!("* {}", rhs);
        let mut sum: f32 = 0.0;
        for i in 0..self.v.len() {
            sum += self.v[i] * rhs.v[i];
        }
        //println!(" = {}", sum);
        return sum;
    }
}

impl fmt::Display for Vector {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for i in 0..self.v.len() {
            write!(f, "{}, ", self.v[i])?;
        }
        return Ok(());
    }
}