use crate::vector::Vector;

use core::fmt;
use std::{usize, vec};

#[derive(Clone)]
pub struct Matrix {
    pub mat: Vec<Vec<f32>>,
    pub rows: usize,
    pub cols: usize,
}

impl Matrix {
    //h: f32, k: f32, 
    pub fn new_empty(rows: usize, cols: usize) -> Matrix {
        let posisjon: Vec<f32> = vec![0.0; cols];
        return Matrix {mat: vec![posisjon; rows], cols, rows};
    }

    pub fn new(mat: Vec<Vec<f32>>) -> Matrix {
        let cols: usize = mat.len();
        let rows: usize =  mat[0].len();
        return Matrix { mat, cols, rows }
    }

    pub fn new_identity(size: usize) -> Matrix{
        let mut new: Matrix = Self::new_empty(size, size);
        for i in 0..size {
            new.mat[i][i] = 1.0;
        }
        return new;
    }

    pub fn is_square(&self) -> Option<usize> {
        if self.cols == self.rows {
            return Some(self.rows);
        } else {
            return None;
        }
    }
}

//Inversmatrise med Gauss-Jordan metoden
impl Matrix {
    #[allow(non_snake_case)]
    pub fn inverse_Gauss_Jordan(self) -> Result<Matrix, MatrixError> {
        /* use std::io::Write;
        let mut file: std::fs::File = OpenOptions::new()
            .append(true).open("tidtakar.txt").unwrap(); */
        //writeln!(file, "-------------------------------").unwrap();
        //let now = std::time::Instant::now();
        //if Self::determinant(&self)? == 0.0 {return Err(MatrixError::DeterminantZero);};
        //writeln!(file, "Det tok {}s aa lage determinanten", now.elapsed().as_secs()).unwrap();
        let matrixes: (Matrix, Matrix) = stairs(self);
        //writeln!(file, "Det tok {}s aa lage trappane", now.elapsed().as_secs()).unwrap();
        let invese_matrixes: (Matrix, Matrix) = scale_diagonal(matrixes.0, matrixes.1);
        //writeln!(file, "Det tok {}s aa rydde over trappane", now.elapsed().as_secs()).unwrap();
        let invese_matrixes: (Matrix, Matrix) = clean_up(invese_matrixes.0, invese_matrixes.1);
        //writeln!(file, "Det tok {}s aa skale midterste rad", now.elapsed().as_secs()).unwrap();
        
        return Ok(invese_matrixes.1);   
        fn stairs(mut mat: Matrix) -> (Matrix, Matrix) {
            let mut indentity_matrix: Matrix = Matrix::new_identity(mat.cols);
            for i in 0..mat.rows {
                let mat_row: Vector = Vector::new(mat.mat[i].clone());
                let id_row: Vector = Vector::new(indentity_matrix.mat[i].clone());
                for j in i+1..mat.cols {
                    if mat.mat[j][i] == 0.0 {continue;}
                    let a = mat.mat[j][i];
                    let scalar: f32 = a / mat.mat[i][i];
                    let scaled_mat_row: Vector = mat_row.mul_scalar(scalar);
                    let scaled_id_row: Vector = id_row.mul_scalar(scalar);
    
                    //Trekker fra den skalerte raden fra baade matrisen og identitetsmatrisen 
                    mat.mat[j] = Vector::new(mat.mat[j].clone()).sub_vector(&scaled_mat_row).v;
                    indentity_matrix.mat[j] = Vector::new(indentity_matrix.mat[j].clone()).sub_vector(&scaled_id_row).v;
                }
            }
            return (mat, indentity_matrix);
        }
    
        fn clean_up(mut mat: Matrix, mut indentity_matrix: Matrix) -> (Matrix, Matrix) {
            for i in (0..mat.rows).rev() {
                let mat_row: Vector = Vector::new(mat.mat[i].clone());
                let id_row: Vector = Vector::new(indentity_matrix.mat[i].clone());
                for j in (0..i).rev() {
                    //indexser j, i
                    if mat.mat[j][i] == 0.0 {continue;}
                    let scalar: f32 = mat.mat[j][i] / mat.mat[i][i];
                    let scaled_mat_row: Vector = mat_row.mul_scalar(scalar);
                    let scaled_id_row: Vector = id_row.mul_scalar(scalar);
    
                    //Trekker fra den skalerte raden fra baade matrisen og identitetsmatrisen 
                    mat.mat[j] = Vector::new(mat.mat[j].clone()).sub_vector(&scaled_mat_row).v;
                    indentity_matrix.mat[j] = Vector::new(indentity_matrix.mat[j].clone()).sub_vector(&scaled_id_row).v;
                }
            }
    
            return (mat, indentity_matrix);
        }
    
        fn scale_diagonal(mut mat: Matrix, mut indentity_matrix: Matrix) -> (Matrix, Matrix) {
            for i in 0..mat.rows {
                let scalar: f32 = 1.0 / mat.mat[i][i];
                mat.mat[i] = Vector::new(mat.mat[i].clone()).mul_scalar(scalar).v;
                indentity_matrix.mat[i] = Vector::new(indentity_matrix.mat[i].clone()).mul_scalar(scalar).v;
            }
            return (mat, indentity_matrix);
        }
    }

}

//Inversematrise med cofaktor metoden
#[allow(dead_code)]
impl Matrix {
    fn inverse_cofactor_method(&self) -> Result<Self, MatrixError> {
        //let now = std::time::Instant::now();
        let det = Self::determinant(self)?;
        //println!("Determinant: {}s", now.elapsed().as_secs());

        //let now = std::time::Instant::now();
        let cofactor_matrix = Self::cofactor_matrix(self.clone());
        //println!("cofactor matrise: {}s", now.elapsed().as_secs());

        //let now = std::time::Instant::now();
        let cofactor_matrix = cofactor_matrix.transpose();
        //println!("transponering: {}s", now.elapsed().as_secs());

        //let now = std::time::Instant::now();
        let inverse = cofactor_matrix.multiply_scalar(1.0/det);
        //println!("skalar multiplikasjon: {}s", now.elapsed().as_secs());
        return Ok(inverse);
    }

    fn minor_matrix(mat: &Matrix, i: usize, j: usize) -> Matrix {
        if mat.cols <= 2 && mat.rows <= 2 {panic!("Det skal ikkje ha vore muleg aa havne her")}
        let mut minor_matrix: Vec<Vec<f32>> = Vec::new();
        for row in 0..mat.rows {
            if row == i {continue;}
            minor_matrix.push(vec![]);
            for col in 0..mat.cols {
                if col == j {continue;}
                let fakk = mat.mat[row][col];
                minor_matrix.last_mut().unwrap().push(fakk);
            }
        }
        return Matrix::new(minor_matrix);
    }

    fn cofactor_matrix(mat: Matrix) -> Matrix {
        let mut cofactor_matrix: Matrix = Self::new_empty(mat.cols, mat.rows);
        for i in 0..mat.rows {
            for j in 0..mat.cols {
                cofactor_matrix.mat[i][j] = Self::cofactor(&mat, i, j);
            }
        }
        return cofactor_matrix;
    }

    fn expanding(mat: &Matrix) -> f32 {
        let mut determinant: f32 = 0.0;
        let first_row: Vec<f32> = mat.mat.get(0).unwrap().clone();
        for index in 0..mat.rows {
            determinant += first_row[index] 
                * Self::cofactor(&mat, 0, index);
        }
        return determinant;
    }

    fn cofactor(mat: &Matrix, i: usize, j: usize) -> f32 {
        let minor_determinant: f32 = 
            Self::determinant(
            &Self::minor_matrix(mat, i, j)).unwrap();

        return (-1 as i32).pow((i+j) as u32) as f32 * minor_determinant;
    }
}

//Determinant
impl Matrix {
    pub fn determinant(&self) -> Result<f32, MatrixError> {
        match self.is_square() {
            Some(_i) => (),
            None => return Err(MatrixError::ShapeMismatch),
        }
        match self.rows {
            1 => return Ok(self.mat[0][0]),
            2 => return Ok(Self::determinant2x2(self).unwrap()), //Skal ikkje kunne feile
            3 => return Ok(Self::determinant3x3(self).unwrap()), //Skal ikkje kunne feile
            _ => return Ok(Self::expanding(self)),
        }
    }
    fn determinant2x2(mat: &Matrix) -> Result<f32, MatrixError> {
        let m: &Vec<Vec<f32>> = &mat.mat;
        if mat.cols != 2 || mat.rows != 2 {return Err(MatrixError::ShapeMismatch);}
        let a: f32 = m[0][0] * m[1][1];
        let b: f32 = m[0][1] * m[1][0];
        return Ok(a-b);
    }

    fn determinant3x3(mat: &Matrix) -> Result<f32, MatrixError> {
        let m: &Vec<Vec<f32>> = &mat.mat;
        if mat.cols != 3|| mat.rows != 3 {return Err(MatrixError::ShapeMismatch);}
        let a: f32 = m[0][0];
        let a_mat: Vec<Vec<f32>> =[m[1][1..=2].to_vec(), m[2][1..=2].to_vec()].to_vec();
        let a_det: f32 = Self::determinant2x2(&Self::new(a_mat)).unwrap();

        let b: f32 = m[0][1];
        let b_mat: Vec<Vec<f32>> = vec![vec![m[1][0], m[1][2]], vec![m[2][0], m[2][2]]];
        let b_det: f32 = Self::determinant2x2(&Self::new(b_mat)).unwrap();

        let c: f32 = m[0][2];
        let c_mat: Vec<Vec<f32>> =[m[1][0..=1].to_vec(), m[2][0..=1].to_vec()].to_vec();
        let c_det: f32 = Self::determinant2x2(&Self::new(c_mat)).unwrap();

        return Ok(a*a_det - b*b_det + c*c_det);
    }
}

//Matrix operations
impl Matrix {
    pub fn multiply_scalar(&self, scalar: f32) -> Matrix {
        let mut scaled_matrix: Matrix = self.clone();
        for i in 0..scaled_matrix.rows {
            for j in 0..scaled_matrix.cols {
                scaled_matrix.mat[i][j] *= scalar;
            }
        }
        return scaled_matrix;
    }

    ///u = Av
    pub fn multiply_vector(&self, vector: &Vec<f32>) -> Vec<f32> {

        //SJEKK AT DEI HAR RETTE FORMAT

        let mut u: Vec<f32> = vec![0.0;self.rows];
        for i in 0..self.rows {
            for j in 0..self.cols {
                u[i] += self.mat[i][j] * vector[j];
            }
        }
        return u;
    }

    pub fn multiply_matrix(&self, matrix: &Matrix) -> Matrix {
        let mut sum_matrix: Matrix = Matrix::new_empty(self.rows, self.cols);
        for i in 0..self.cols {
            for j in 0..self.cols {
                //print!("{},{}: ", i, j);
                let lhs = self.get_row(i).unwrap();
                let rhs = matrix.get_col(j).unwrap();
                let dot_product: f32 = 
                    lhs
                    .dot_product(rhs);
                sum_matrix.mat[i][j] = dot_product;
            }
            //print!("\n")
        }
        return sum_matrix;
    }

    pub fn multiply_matrix_without_border(&self, matrix: &Matrix) -> Matrix {
        let mut sum_matrix: Matrix = Matrix::new_empty(self.cols, self.cols);
        for i in 1..self.cols-1 {
            for j in 1..self.cols-1 {
                let mut dot_product: f32 = 0.0; 
                    self.get_row(i).unwrap()
                    .dot_product(matrix.get_col(j).unwrap());
                sum_matrix.mat[i][j] = dot_product;
            }
        }
        return sum_matrix;
    }

    pub fn add_matrix(mut self, rhs: Matrix) -> Matrix {
        for i in 0..self.rows {
            for j in 0..self.cols {
                self.mat[i][j] += rhs.mat[i][j];
            }
        }
        return self;
    }

    pub fn transpose(&self) -> Matrix {
        let mut transposed_matrix: Matrix = Self::new_empty(self.cols, self.rows);
        for i in 0..self.rows {
            for j in 0..self.cols {
                transposed_matrix.mat[j][i] = self.mat[i][j];
            }
        }
        return transposed_matrix;
    }

    pub fn sum(&self) -> f32 {
        let mut sum: f32 = 0.0;
        for i in 0..self.rows {
            for j in 0..self.cols {
                sum += self.mat[i][j];
            }
        }
        return sum;
    }

    pub fn set_row(&mut self, value: f32, row: usize) {
        for i in 0..self.rows {
            self.mat[row][i] = value;
        }
    }
    
    pub fn set_col(mut self, value: f32, col: usize) -> Matrix {
        for i in 0..self.cols {
            self.mat[i][col] = value;
        }
        return self;
    }

}

//Aksesseringar
impl Matrix {
    pub fn get_row(&self, row: usize) -> Option<Vector>{
        return Some(Vector::new(self.mat.get(row)?.clone()));
    }
    pub fn get_col(&self, col: usize) -> Option<Vector> {
        let mut column: Vec<f32> = Vec::new();
        for i in 0..self.cols {
            column.push(self.mat[i][col]);
        }
        return Some(Vector::new(column));
    }
}

impl fmt::Display for Matrix {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for i in 0..self.rows {
            for j in 0..self.cols {
                write!(f, "{val:.*}", 2, val = self.mat[i][j])?;
                if j+1 != self.cols {write!(f, ",\t")?;}
            }
            if i+1 != self.rows {write!(f, "\n")?;}
        }
        return Ok(());
    }
}

#[cfg(test)]
mod tests {
    use crate::matrix::Matrix;

    #[test]
    fn determinant() {
        let mat: Vec<Vec<f32>> = vec![  
            vec![1.0, 3.0, -2.0, 1.0],
            vec![5.0, 1.0, 0.0, -1.0],
            vec![0.0, 1.0, 0.0, -2.0],
            vec![2.0, -1.0, 0.0, 3.0]];
        
        let mat: Matrix = Matrix::new(mat);
        /* println!("{}", mat.clone());
         let det: Result<f32, matrix::MatrixError> = TrueMatrix::determinant(mat.clone());
        println!("{:?}", det);
        let cofactor_mat: TrueMatrix = TrueMatrix::cofactor_matrix(mat.clone());
        println!("{}", cofactor_mat);
        let transposed_mat: TrueMatrix = mat.transpose();
        println!("{}", transposed_mat);
        let inverse: Matrix = mat.inverse().unwrap(); */
        assert_eq!(Matrix::determinant(&mat).unwrap(), -6.0);
    }

    #[test]
    fn multiplication() {
        let vec0 = vec![vec![1.0,2.0,3.0], vec![4.0,5.0,6.0]];
        let vec1 = vec![vec![7.0,8.0], vec![9.0,10.0], vec![11.0,12.0]];

        let mat0 = Matrix::new(vec0);
        let mat1 = Matrix::new(vec1);

        let mat2 = mat0.multiply_matrix(&mat1);

        let det = Matrix::determinant(&mat2).unwrap();
        assert_eq!(32.0, det)
    }

    #[test]
    fn time_test() {
        let mat: Vec<Vec<f32>> = vec![vec![3.0;512];512];
        let matrix01 = Matrix::new(mat.clone());
        let matrix02 = Matrix::new(mat);
        let now = std::time::Instant::now();
        let _matrix03 = matrix01.multiply_matrix(&matrix02);
        println!("{}", now.elapsed().as_millis());

    }
}

#[derive(Debug)]
pub enum MatrixError {
    ShapeMismatch,
    DeterminantZero,
}

impl fmt::Display for MatrixError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::ShapeMismatch => write!(f, "Denne funksjonen kan ikkje behandle ein matrise med denne stroerrelsen"),
            Self::DeterminantZero => write!(f, "Determinanten er 0, denne operasjonen kan difor ikkje nyttast"),
        }
        
    }
}