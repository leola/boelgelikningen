import matplotlib.pyplot as plt
import numpy as np

from matplotlib import cm
import matplotlib.colors as colors
from matplotlib import animation

Zt = []

with open("bolgelikningen.txt", "r") as file:
    csv = file.read()
    csv = csv.split("|")
    #csv.pop(-1)

    info = csv[0].split(',')
    csv.pop(0)
    
    for block in csv:
        Z = []
        mat = block.split('\n')
        for row in mat:
            line = row.split(",")
            z = []
            for num in line:
                if num == ' ' or num == '':
                    continue
                z.append(float(num))
            Z.append(z)
        Z.pop(0)
        Zt.append(Z)
Zt.pop(-1)

x0_points = int(info[0])
x1_points = int(info[1])
t_points = int(info[2])
h = float(info[3])
k = float(info[4])

X0 = np.arange(0, x0_points*h, h)
X1 = np.arange(0, x1_points*h, h)
X0, X1 = np.meshgrid(X0, X1)
Zt = np.array(Zt)



def animate3d():
    animation_time_s = 2
    total_frames = t_points
    fps = int(total_frames / animation_time_s)
    

    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    surface = [ax.plot_surface(X0, X1, Zt[0,:], cmap=cm.coolwarm)]
    min, max = Zt.min()/2, Zt[1,:].max()/2

    def init():
        ax.set_zlim(min, max)
        ax.view_init(elev=50, azim=-128, roll=0)
        return surface,

    def update(frame, Zt, surface):
        surface[0].remove()
        surface[0] = ax.plot_surface(X0, X1, Zt[frame-1,:], cmap=cm.coolwarm, vmin = min, vmax = max)
        #surface[0].set_zlim(min, max)
        return surface,

    animator = animation.FuncAnimation(fig, update, init_func=init,
                                       frames=np.linspace(0, t_points, total_frames).astype(int), 
                                       fargs=(Zt, surface),
                                       interval = 1000/fps)
                                       
    #animator.save("bolge.gif")
    plt.show()

""" fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
ax.plot_surface(X0, X1, Zt[0], cmap=cm.coolwarm) """

animate3d()